NCBI database updater
=======================================

Description
-----------

The NCBI database updater is a highly configurable python program that allows
downloading and maintaining a local copy of NCBI databases for further
downstream use.

Features:
   
 * Select NCBI databases to download
 * Configure sequence filters to specifically pick the sequences you need
 * Support for indexing for several alignment tools.
 * Split the databases into user-definable subsets
 * Taxonomically annotate all sequences for metagenomic profiling

Get the NCBI database updater
-----------

Please visit https://gitlab.com/andreas.andrusch/database-updater to find the
latest source code and documentation.
